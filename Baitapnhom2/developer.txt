MSSV: 12110235
Trang web:(http://lmhtmoba.vn) :xây dựng website về một thể loại game thể thao điện tử Liên Minh Huyền Thoại. Cung cấp thông tin về các giải đấu ,tin tức ,khuyến mãi ,giới thiệu ,hình ảnh, …thu hút người chơi về thể loại game moba đã tồn tại từ lâu.Giúp người chơi nhận thức rằng game không đơn thuần chỉ để giải trí mà còn là một môn thể thao trí tuệ .

Mục đích:
@Ðối với người dùng:
_Tạo ra một sân chơi mới với thể loại game moba đã tồn tại từ lâu nhưng không nhàm chán và Phù hợp với mọi lứa tuổi cũng như đa dạng hóa thành phần người chơi(moba là thể loại game đơn giản đã và đang rất thu hút giới trẻ trong và ngoài nước với rất nhiều các game khác nhau mà nỗi bật cần nhắc đến là lienminhhuyenthoai có tên nước ngoài là league of legends ).
_Cung cấp và cập nhật liên tục những thông tin liên quan đếm game để người chơi cập nhật những thông tin cần thiết cung như tin tức ngoài lề về game mình đang chơi.
_Cung cấp và cập nhật thông tin về các giải đấu trong và ngoài nước để người chơi học hỏi cách chơi của những người chơi bậc cao trong nước và thế giới đồng thời thưởng thức những trận đấu đỉnh cao của các game thủ siêu sao quốc tế.
_Cung cấp video & hình ảnh về ngoại trang, lịch sử, cốt truyện và cách chơi cho từng tướng nhằm giúp người chơi hiểu rõ hơn về tướng đó. Không đơn thuần chỉ tồn tại một tướng mới mà hình thành song song với nó là cốt truyện nhằm gắn kết nó với phần còn lại của game ,tạo sự gắn kết mạch lạc trong game cũng như thu hút về những điều bí ẩn xung quanh vị tướng vừa xuất hiện.
_Tạo ra một cộng đồng người chơi lành mạnh. Nhận thức đúng đắng về bản chất của thể loại game moba. Chơi game vừa để giải trí sau những giờ làm việc, học tập căng thẳng vừa phát triển các kỹ năng bản thân như một hình thức rèn luyện trí tuệ và khả năng làm việc nhóm .
_Gắn kết mọi người lại với nhau bằng hình thức chơi game thoải mái.
_Tạo nơi chia sẽ kinh ngiệm, trao đỗi về các vấn đề liên quan trong game.
@Ðối với nhà đầu tư:
_Khi đầu tư vào trang web này các nhà đầu tư sẽ xác định cho mình được những lợi ích trước mắt và lâu dài nhờ vào số lượng người chơi thể loại game moba đơn giản nói chung cũng như liên minh huyền thoại nói riêng đang ngày một tăng. 
_Trang Web còn tồn tại các cấp bậc cho các nhà đầu tư nhằm phân chia rõ vùng lợi nhuận và mức độ lợi nhuận để các nhà đầu tư xác định một cách rõ rệt về các khoãng mình đã và sắp đầu tư.


Cách hoạt động:
_Mỗi người dùng sẽ có một tài khoản riêng cho mình nhằm xác thực các quyền trên trang web
_Mỗi người dùng sẽ được cập nhật một cách liên tục và chính xác nhất về những thông tin liên quan đến game.
_Người dùng có thể tạo tài khoản và tham gia vào các hoạt động trong game như để lại ý kiến ,tham khảo ,bình luận về các vấn đề hiễn thị ờ mục cộng đồng.
_Người dùng củng có thể đăng bài nhằm giải quyết những thắc mắc của bản thân về quá trính trải nghiệm.
_Những bài đăng hay được nhiều lượt like sẽ được lên top trang chủ, đồng thời những bài đăng vi phạm sẽ bị ban theo quy đinh của ban quản lý bài đăng.
_Thông qua trang web có thể nạp RP cho game.

Thu lợi nhuận:
_Hợp tác với một số trang web liên quan nhằm phát triển về tài chính và nôi dung liên quan.
_Nhà đầu tư sẽ thu lợi nhuận từ các quảng cáo đính kèm tùy thuộc vào vị trí và kích thước của quảng cáo.
_Phần nạp thẻ cho tài khoản cá nhân của các game thủ.
_Thu lợi từ lượt view với các trang liên kết trên facebook hay youtube ...


