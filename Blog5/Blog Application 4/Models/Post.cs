﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_Application_4.Models
{
    public class Post
    {
        public int PostID { get; set; }
        [Required]
        [StringLength(500, ErrorMessage = "Title có từ 20 đến 500 ký tự !!", MinimumLength = 20)]
        public String Title { get; set; }
        [Required]
        [StringLength(500, ErrorMessage = "Nội dung chứa ít nhất 50 ký tự !!", MinimumLength = 50)]
        public String Body { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime DateUpdated { get; set; }

        //
        public int UserProfileUserId { get; set; }
        public virtual UserProfile UserProfile { get; set; }
        //
        public virtual ICollection<Comment> Comments { get; set; }
        //
        public virtual ICollection<Tag> Tags { get; set; }
    }
}