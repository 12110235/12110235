﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_Application_4.Models
{
    public class Comment
    {
        public int CommentID { get; set; }
        [Required]
        public String Author { get; set; }
        [Required]
        [StringLength(500, ErrorMessage = "Nội dung chứa ít nhất 50 ký tự !!", MinimumLength = 50)]
        public String Body { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime DateUpdated { get; set; }

        public int LastComment
        {
            get
            {
                return (DateTime.Now - DateCreated).Minutes;
            }
        }

        //
        public int PostID { get; set; }
        public virtual Post Post { get; set; }
    }
}